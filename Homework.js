//Untuk generate Array secara random
let randomArray = [];
for (let i = 0; i < 100; i++) {
    randomArray.push(Math.floor(Math.random() * 50) + 1);
}

let evenArray = [];
let oddArray = [];

//Sorting Array ganjil dan genap
for (let i = 0; i < randomArray.length; i++) {
    if (randomArray[i] % 2 === 0) {
        evenArray.push(randomArray[i]);
    } else {
        oddArray.push(randomArray[i]);
    }
}

//Fungsi nilai max
function max(arr){
    if (arr.length === 0) {
        return undefined; // Return undefined jika array kosong
    }

    let max = arr[0]; // Anggap elemen pertama sebagai nilai maksimal awal

    for (let i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i]; // Perbarui nilai maksimal jika ditemukan nilai yang lebih besar
        }
    }

    return max;
}


//Fungsi nilai min
function min(arr) {
    if (arr.length === 0) {
        return undefined; // Return undefined jika array kosong
    }

    let min = arr[0]; // Anggap elemen pertama sebagai nilai minimal awal

    for (let i = 1; i < arr.length; i++) {
        if (arr[i] < min) {
            min = arr[i]; // Perbarui nilai minimal jika ditemukan nilai yang lebih kecil
        }
    }

    return min;
}

//Fungsi nilai total
function total(arr) {
    let total = 0;

    for (let i = 0; i < arr.length; i++) {
        total += arr[i];
    }

    return total;
}

//Fungsi rata-rata
function rata(arr) {
    if (arr.length === 0) {
        return undefined; // Return undefined jika array kosong
    }

    let total = 0;

    for (let i = 0; i < arr.length; i++) {
        total += arr[i]; // Tambahkan setiap nilai pada array ke total
    }

    return total / arr.length; // Kembalikan rata-rata
}

//Perbandingan
let banding = "";
if(max(evenArray) > max(oddArray)){
    banding += "Max lebih besar pada Array Genap\n";
} else if(max(evenArray) < max(oddArray)) {
    banding += "Max lebih besar pada Array Ganjil\n"
} else {
    banding += "Max memiliki nilai yang sama antara array genap dan ganjil\n";
}

if(min(evenArray) > min(oddArray)){
    banding += "Min lebih besar pada Array Genap\n";
} else if(min(evenArray) < min(oddArray)) {
    banding += "Min lebih besar pada Array Ganjil\n"
} else {
    banding += "Min memiliki nilai yang sama antara array genap dan ganjil\n";
}

if(total(evenArray) > total(oddArray)){
    banding += "Total lebih besar pada Array Genap\n";
} else if(total(evenArray) < total(oddArray)) {
    banding += "Total lebih besar pada Array Ganjil\n"
} else {
    banding += "Total memiliki nilai yang sama antara array genap dan ganjil\n";
}

if(rata(evenArray) > rata(oddArray)){
    banding += "Rata lebih besar pada Array Genap\n";
} else if(rata(evenArray) < rata(oddArray)) {
    banding += "Rata lebih besar pada Array Ganjil\n"
} else {
    banding += "Rata memiliki nilai yang sama antara array genap dan ganjil\n";
}

const evenStats = "";
const oddStats = "";

console.log("======================================================================================");
console.log("Random Array:", randomArray);
console.log("======================================================================================");
console.log("Array Genap:", evenArray);
console.log("======================================================================================");
console.log("Array Ganjil:", oddArray);
console.log("======================================================================================");
console.log("Max di Array Genap :", max(evenArray) + " | " + "Max di Array Ganjil :", max(oddArray));
console.log("Min di Array Genap :", min(evenArray) + " | " + "Min di Array Ganjil :", min(oddArray));
console.log("Total di Array Genap :", total(evenArray) + " | " + "Total di Array Ganjil :", total(oddArray));
console.log("Average di Array Genap :", rata(evenArray) + " | " + "Average di Array Ganjil :", rata(oddArray));
console.log("======================================================================================");
console.log("", banding);